package com.capgemini.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.training.customer.entity.Address;
import com.capgemini.training.customer.entity.Customer;
import com.capgemini.training.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	@GetMapping("/customers")
	public List<Customer> getAllCustomer() {
		return customerService.getAllCustomer();
	}
	
	@GetMapping("/customers/{id}")
	public Customer getCustomer(@PathVariable Long id) {
		return customerService.getCustomer(id);
	}
	
	@PostMapping("/customers")
	public void createCustomer(@RequestBody Customer cust) {
		customerService.save(cust);
	}
	
	@PutMapping("/updatecustomers/{id}")
	public void updateCustomer(@PathVariable Long id) {
		customerService.update(id);
	}
	
	@DeleteMapping("/customers/{id}")
	public void deleteCustomer(@PathVariable Long id) {
		customerService.delete(id);
	}
	
	@GetMapping("/customers/{id}/addresses")
	public List<Address> getCustomerAddress(@PathVariable Long id)
	{
		return customerService.getCustomerAddress(id);
	}
	
	@PutMapping("/customers/addresses/{id}")
	public void updateCustomerAddress(@PathVariable Long id)
	{
		customerService.updateAddress( id);
	}
	
	@GetMapping("/customers/address/{cId}/{aId}/retrive")
	public Address getCustomerAddressById(@PathVariable Long cId,@PathVariable Long aId)
	{
		return customerService.getCustomerAdressById(cId, aId);
	}
	
	@PutMapping("/customers/{cId}/addresses/{aId}/update")
	public Customer updateCustomerAddressById(@RequestBody Address address,@PathVariable Long cId, @PathVariable Long aId )
	{
		return customerService.updateAddressById(address, cId,aId);
	}
}
