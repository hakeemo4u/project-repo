package com.capgemini.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.training.customer.entity.Address;
import com.capgemini.training.service.AddressService;

@RestController
@RequestMapping("/api")
public class AddressController {
	
	@Autowired
	AddressService addressService;
	
	@GetMapping("/address")
	public List<Address> getAllAddress() {
		return addressService.getAllAddress();
	}
	
	@GetMapping("/address/{id}")
	public Address getAddress(@PathVariable Long id) {
		return addressService.getAddress(id);
	}
	
	@PostMapping("/customers/address")
	public void createAddress(@RequestBody Address addr) {
		addressService.save(addr);
	}
	
	@PutMapping("/customers/address/{id}")
	public void updateAddress(@PathVariable Long id) {
		addressService.update(id);
	}
	
	@DeleteMapping("/address/{id}")
	public void deleteAddress(@PathVariable Long id) {
		addressService.delete(id);
	}

}
