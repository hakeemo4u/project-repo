package com.capgemini.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capgemini.training.customer.entity.Communication;

public interface CommunicationRepository extends JpaRepository<Communication, Long>{

}
